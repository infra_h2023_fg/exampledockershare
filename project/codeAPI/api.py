from flask import *
import os

app = Flask("test")
UPLOAD_FOLDER = "/mnt/app/files/"

@app.route("/")
def hello():
	return "Hello everyone !!!"

@app.route("/upload", methods=['POST'])
def upload():
	if 'file' not in request.files:
		abort(400, "file missing from request")
	myFile = request.files['file']
	fileName = "uploadedFile.patate"
	myFile.save(os.path.join(UPLOAD_FOLDER, fileName))
	return "file uploaded successfully with name: " +  fileName

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=5555)
