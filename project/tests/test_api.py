import unittest

from codeAPI import api

class BasicTests(unittest.TestCase):

	def test_hello_route(self):
		tester = api.app.test_client(self)
		response = tester.get('/')
		self.assertEqual(200, response.status_code)
		self.assertIn(b'Hello', response.data)

if __name__ == '__main__':
	unittest.main()
