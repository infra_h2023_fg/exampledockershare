import requests
import sys

def uploadFile(filename):
	f = open(filename, "rb") #reading file in binary mode

	myFile = {'file': f}

	resp = requests.post("http://127.0.0.1:5556/upload", files=myFile)
	print(resp.content.decode('utf-8')) 
	print("status code: " + str(resp.status_code))


if __name__ == '__main__':
	if len(sys.argv) == 2:
		fileName = sys.argv[1]
		print("uploading file: " + fileName)
		uploadFile(fileName)
	else:
		print("This program needs to run with exactly 1 argument being the full file name to upload.")
		exit(1)

